package cn.pangpython.acl.proxy;
/**
 * 用户数据库操作接口类
 * @Project ApacheCommonsLearn
 * @Package cn.pangpython.acl.proxy
 * @Author pangPython
 * @Time 上午8:33:47
 */
public interface UserDao {
	/*
	 * 登录方法
	 */
	public boolean login(String UserName,String PassWord);
	
}
