package cn.pangpython.acl.proxy;

import java.lang.reflect.Proxy;

/**
 * @Project ApacheCommonsLearn
 * @Package cn.pangpython.acl.proxy
 * @Author pangPython
 * @Time 下午3:45:11
 */
public class TestProxy {

	public static void main(String[] args) {
			UserDaoProxy userDaoProxy = new UserDaoProxy(new UserDaoImpl());
			UserDao userDao = (UserDao) Proxy.newProxyInstance(UserDaoImpl.class.getClassLoader(), UserDaoImpl.class.getInterfaces(), userDaoProxy);
			userDao.login("test", "test");
	}

}
