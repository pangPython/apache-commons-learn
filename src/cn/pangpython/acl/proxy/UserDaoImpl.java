package cn.pangpython.acl.proxy;
/**
 * @Project ApacheCommonsLearn
 * @Package cn.pangpython.acl.proxy
 * @Author pangPython
 * @Time 上午8:38:26
 */
public class UserDaoImpl implements UserDao {

	@Override
	public boolean login(String UserName, String PassWord) {
		System.out.println("用户名："+UserName+"密码："+PassWord+" ===正在登录！");
		return true;
	}

}
