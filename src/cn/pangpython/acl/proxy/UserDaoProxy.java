package cn.pangpython.acl.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @Project ApacheCommonsLearn
 * @Package cn.pangpython.acl.proxy
 * @Author pangPython
 * @Time 上午8:41:03
 */
public class UserDaoProxy implements InvocationHandler {

	private Object target;
	
	public UserDaoProxy(Object target) {
		super();
		this.target = target;
	}

	@Override
	public Object invoke(Object object, Method method, Object[] args) throws Throwable {
		System.out.println("检查登录参数...");
		Object obj = method.invoke(target, args);
		System.out.println("记录登录日志...");
		return obj;
	}

}
