package cn.pangpython.acl.net;

import java.io.IOException;

import org.apache.commons.net.whois.WhoisClient;

/**
 * @Project ApacheCommonsLearn
 * @Package cn.pangpython.acl.net
 * @Author pangPython
 * @Time 下午3:44:56
 * 域名注册查询
 */
public class WhoisTest {
	public static void main(String[] args) {
		WhoisClient whois = new WhoisClient();

		    try {
		    	//连接whois查询服务器
		    	//默认 whois.internic.net 端口43
		      whois.connect(WhoisClient.DEFAULT_HOST);
		      //查询
		      System.out.println(whois.query("oschina.net"));
		      //关闭连接
		      whois.disconnect();
		      //使用国家域名whois服务器
		      whois.connect("whois.cnnic.cn");
		      System.out.println(whois.query("12306.cn"));
		      whois.disconnect();
		    } catch(IOException e) {
		      System.err.println("Error I/O exception: " + e.getMessage());
		      return;
		    }
	}
}
