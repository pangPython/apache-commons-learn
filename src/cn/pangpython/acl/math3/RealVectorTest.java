package cn.pangpython.acl.math3;

import org.apache.commons.math3.linear.ArrayRealVector;
import org.apache.commons.math3.linear.RealVector;

/**
 * @Project ApacheCommonsLearn
 * @Package cn.pangpython.acl.math3
 * @Author pangPython
 * @Time 下午4:46:09
 */
public class RealVectorTest {
	public static void main(String[] args) {
		//创建向量
		double[] value1 = {1,-1,2,0};
		double[] value2 = {1,1,-1,0};
		RealVector vector1 = new ArrayRealVector(value1);
		RealVector vector2 = new ArrayRealVector(value2);
		//输出
		System.out.println("vector1:	"+vector1.toString());
		//长度
		System.out.println("vector1 length: "+value1.length);
		System.out.println("vector2:	"+vector2.toString());
		//相加
		System.out.println("vector1+vector2:"+vector1.add(vector2));
	}
}
