package cn.pangpython.acl.collection4;

import org.apache.commons.collections4.IterableMap;
import org.apache.commons.collections4.MapIterator;
import org.apache.commons.collections4.map.HashedMap;

/**
 * @Project ApacheCommonsLearn
 * @Package cn.pangpython.acl.collection4
 * @Author pangPython
 * @Time 上午8:12:26
 */
public class IterableMapTest {
	public static void main(String[] args) {
		//创建方便遍历的map
		IterableMap<Object,Object> map = new HashedMap<>();
		//给map中添加数据
		map.put("lang", "java");
		map.put("project", "apache commons collections4");
		map.put("version", 4.1);
		//迭代器
		MapIterator<Object, Object> it = map.mapIterator();
		//遍历
		while (it.hasNext()) {
		  Object key = it.next();
		  Object value = it.getValue();
		  System.out.println("Key:"+key+" Value:"+value);
		}
		
	}
}
